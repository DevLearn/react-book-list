
export default function () {
  return [
    {title: 'The Dark Tower', pages: 301},
    {title: 'Inferno', pages: 350},
    {title: 'Point of impact', pages: 280}
  ];
}
